**Michal and Company Geode Decor**

Geode décor can bring a sense of style into your home, At Michal&Co we specialize in functional geode decor art pieces that spectacularly accent key spaces creating opulence, & life in abundance. We are a direct importer and wholesale company with design and manufacturing facilities that incorporate the use of natural crystals, gems, geode rocks, minerals and fossils Nature’s Art™ into geode décor accents for your home, corporate or landscape environments as Nature’s Art Décor.™

*[https://michalandcompany.com/home-geode-decor/](https://michalandcompany.com/home-geode-decor/)